from flask_frozen import Freezer
from app import app

freezer = Freezer(app)


@freezer.register_generator
def product_url_generator():  # Some other function name
    types = ["modern", "premium", "luxary"]
    ids = ["1bhk", "2bhk", "3bhk"]
    # `(endpoint, values)` tuples
    for type in types:
        for id in ids:
            yield 'details', {'type': type, 'id': id}

@freezer.register_generator
def pricing():
    types = ["modern", "premium", "luxary"]
    for type in types:
        yield {'type': type}

@freezer.register_generator
def landing():
    choices = ['arch', 'glazing', 'interior', 'project', 'scaping', 'virtual']
    for choice in choices:
        yield {'what': choice}

@freezer.register_generator
def gallery():
    types = ['arch', 'glazing', 'interior', 'landscaping']
    for type in types:
        yield {'type': type}

if __name__ == '__main__':
    freezer.freeze()