def Premium():

    PremiumList = [
        {
            'id': "1bhk",
            'name': "Premium - 1BHK",
            'price': "2,70,000",
            'description': "This is our premium package for 1BHK Homes which comes with premium goodies at an amazing price point",
            'category': "Premium"
        },
        {
            'id': "2bhk",
            'name': "Premium - 2BHK",
            'price': "3,65,000",
            'description': "This is our premium package for 2BHK Homes which comes with premium goodies at an amazing price point",
            'category': "Premium"
        },
        {
            'id': "3bhk",
            'name': "Premium - 3BHK",
            'price': "4,85,000",
            'description': "This is our premium package for 3BHK Homes which comes with premium goodies at an amazing price point",
            'category': "Premium"
        }
    ]

    return PremiumList


