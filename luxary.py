def Luxary():

    LuxaryList = [
        {
            'id': "1bhk",
            'name': "Luxary - 1BHK",
            'price': "5,45,000",
            'description': "This plan includes everything that you ever asked for and more to turn your 1BHK Home or Office into a palace.",
            'category': "Luxary"
        },
        {
            'id': "2bhk",
            'name': "Luxary - 2BHK",
            'price': "10,85,000",
            'description': "This plan includes everything that you ever asked for and more to turn your 2BHK Home or Office into a palace.",
            'category': "Luxary"

        },
        {
            'id': "3bhk",
            'name': "Luxary - 3BHK",
            'price': "16,20,000",
            'description': "This plan includes everything that you ever asked for and more to turn your 3BHK Home or Office into a palace.",
            'category': "Luxary"
        }
    ]

    return LuxaryList


