def Modern():

    modernList = [
        {
            'id': "1bhk",
            'name': "Modern - 1BHK",
            'price': "1,95,000",
            'description': "This package is affordable yet It with comes all the goodies that satisfies your dream to create a beautiful home.",
            'category': "Modern"
        },
        {
            'id': "2bhk",
            'name': "Modern - 2BHK",
            'price': "2,55,000",
            'description': "This package is affordable yet It with comes all the goodies that satisfies your dream to create a beautiful home.",
            'category': "Modern"

        },
        {
            'id': "3bhk",
            'name': "Modern - 3BHK",
            'price': "3,15,000",
            'description': "This package is affordable yet It with comes all the goodies that satisfies your dream to create a beautiful home.",
            'category': "Modern"
        }
    ]

    return modernList


