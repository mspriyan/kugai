title: Modern - 1BHK
date: 2019-02-07

**ENTRANCE**

* Shoe rack - 100*75 cm

**LIVING ROOM**

* Premium LCD display unit

**DINING ROOM**

* 6 Seater dining table with 12 mm clear glass top
* Hylo dining chair - 3 Nos
* 3 Seater dining bench - 1 No

**MASTER BEDROOM**

* Soft close wardrobe - 120 * 210 cm
* King size bed with bottom storage - 187*198 cm
* Bed side table (2 No) - 40*40

**KITCHEN**

* Top cabinet - 304*60 cm, Bottom cabinet - 304x90 cm
* OLIVE (German Made - 15 year warranty) Accessories - 6 Nos

**OLIVE Premium Soft close Accessories(60cm)**

* Cutlary tray
* Plain basket
* Plate rack
* Bottle pull out
* Waste bin
* Detergent holder
