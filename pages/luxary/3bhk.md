title: Luxary - 3BHK
date: 2019-02-07

**ENTRANCE**

* Shoe rack and ledges - 100 * 75 cm / 90 * 2 Nos
* Brass finish mirror wall art - 1No
* Wall paper -2 rolls
* Pendent lights -1(set of 2) Artificial plants -1(set of 3)

**LIVING ROOM**

* TV unit with black glass shutters -180*160 cm
* Prayer unit -70*90 cm
* Sheer :GMF homes sky sheers -1 No
* Off-White wood grain Wall paneling -1 No
* Decorative roman clock -1 NoArtificial plants -1(set of 2)
* Sofa fiori L shape 3 seater + lounge -1 No
* Carpet -200*140 cm
* Additional cushions for sofa -4 Nos
* Centre table -1 No

**DINING ROOM**

* 6 Seater dining table with 12 mm clear glass top
* Hylo dining chair -3 Nos 3
* Seater dining bench -1 No
* Custom-made living-dining partition -180 *210 cm
* Sheer : GMF homes sky sheers : Sheer without lining -1 No
* Artificial plants small size (setof 3) -1 No
* Wall paper -2 rolls

**MASTER BEDROOM**

* Soft - close hinged wardrobe upto ceiling - 150*270 cm
* Dressing unit -60*210 cm
* Custom-made writing tableBooks cabinet with paneling and ledges -135*150 cm
* King size bed with bottom storage -187*198 cm
* Bed side table (2 No) -40*40 cm
* Curtain: GMF ATRANI -1 No
* Single chair yellow -1 No
* Wall paper -4 Nos
* Wall picture horizontals -1 No
* Mattress Peps 5" -1 No
* Bed setting -1 No

**GUEST ROOM**

* Soft-close hinged Wardrobe -150*210 cm
* Queen size bed with bottom storage -157*198 cm
* Bed side table (2 No) -40*40 cm
* Mattress Peps 5'' -1 No
* Bed setting -1 No

**KIDS ROOM**

* Soft-close hinged wardrobe upto ceiling -150*270 cm
* Custom-made writing table
* Books cabinet with paneling and ledges -135*150 cm
* Queen size bed with bottom storage 157*198 cm
* Bed side table (2 No) -40*40 cm

**KITCHEN**

* Top cabinet -457*60 cm
* Bottom Cabinet -457*85 cm
* Hettichor SLEEK(German Made -15 year warranty) Accessories -6 Nos*

**Hettich or SLEEK**
* **Premium Soft close Accessories(60cm)**

* Cutlary tray
* Plain basket
* Plate rack
* Bottle pull out
* Waste bin - Pull out Model
* Detergent holder