title: Premium - 1BHK
date: 2019-02-07

**ENTRANCE**

* Shoe rack and ledges - 100 * 75 cm / 90 * 2 Nos

**LIVING ROOM**

* TV unit with black glass shutters - 180*160 cm
* Prayer unit - 70*90 cm

**DINING ROOM**

* 6 Seater dining table with 12 mm clear glass top
* Hylo dining chair - 3 Nos
* 3 Seater dining bench - 1 No
* Custom-made living - dining partition - 180 *210 cm

**MASTER BEDROOM**

* Soft - close hinged wardrobe upto ceiling - 150*270 cm
* Dressing unit - 60*210 cm
* Custom-made writing table
* Books cabinet with paneling and ledges - 135*150 cm
* King size bed with bottom storage - 187*198 cm
* Bed side table (2 No) - 40*40 cm

**KITCHEN**

* Top cabinet - 457*60 cm
* Bottom cabinet - 457*85 cm
* HAFELE (German Made - 15 year warranty) Accessories -6 Nos*

**HAFELE Premium Soft close Accessories(60cm)**

* Cutlary tray
* Plain basket 
* Late rack
* Bottle pull out
* Waste bin -Pull out Model
* Detergent holder
