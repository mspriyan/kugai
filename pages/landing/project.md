title: Project Management
banner: /static/images/banner/project.jpg

##OUR SERVICES
####We are the catalyst that takes a project from start to finish.

###  CONCEPTION
**Budget/Schedule Administration** -
We monitor budget and schedule to ensure project is on track until completion.
**Development Management** -
Represent your interests in all aspects of out-of-the-ground development
**Cost Estimating** -
Create and develop total project costs, inclusive of hard and soft costs

##PLANNING
Examine potential sites through site visits and cost analysis. Ensure contracts represent best interests and practices
**Site Selection &amp; Due Diligence** -
Examine potential sites through site visits and cost analyses
**Contract Negotiations** -
Ensure all contracts represent your best interest and industry best practices
**Facilities Support Services** -
Track and manage facilities operations requests, lease administration, costs, and produce reports
**Contract Administration** -
Verify that vendors are complying with negotiated terms of contract throughout the life of the project

##DESIGN
From concept to completion. Watchdog offers the full spectrum of project management and owners representative services.
**FF&amp;E Management** -
Implement budget creation, RFP production, procurement process, and oversee from planning through installation
**Compliance** -
Monitor design based on the established budget and schedule
**Owner-Vendor Coordination** -
Facilitate communication between architect, AV &amp; IT providers, and relay all data.
**Ensure Lease Alignment** -
Ensure the designers and project team are designing/implementing per lease specifications

##CONSTRUCTION
Implement budget creation, RFP production, monitor compliance, and ensure lease alignment
**Construction Administration** -
Oversee all construction activities, mitigate costs, verify who owns which expense, compare contract documents to as-built conditions in the field, manage/negotiate change orders and, in general, oversee all financial aspects of the project
**Payment Application Review** -
Review all GC monthly payments for compliance and as-built conditions in the field.
**Work Letter Analysis** -
Review work letter to ensure your best interests are being served

##RELOCATION
Assist in delivering consistent results for multisite facilities from beginning to end, and oversee physical move
**Multisite Rollout Management** -
Assist in delivering consistent results for multisite facilities from beginning to end
**Relocation** -
Comprehensive planning and on-site oversight of physical move

##CLOSEOUT
Help clients develop a long-term expenditure plan and deliver all manuals and lien waivers to owner.
**Capital Budget Planning** -
Help clients develop a long-term expenditure plan
**Closeout Management** -
Deliver all manuals and lien waivers to owner, ensure all parties are paid, and mitigate post-occupancy issues.

## Construction Project Management Services

Nova&#39;s scope of services is tailored to match each client&#39;s specific needs. Our focus is on efficient and effective management, which is aimed at protecting the interests of our client.

Our staff has managed the construction of a variety of facilities for a multitude of clients: educational and municipal facilities, health care facilities, firehouses, community centers, libraries, industrial and wastewater treatment plants, bridges, roadways and housing projects to name a few.

The Nova&#39;s team of seasoned construction managers assigned to each project takes pride in the services they provide. Our management teams provide project leadership and maintain a focus on the construction schedule. They effectively communicate with the project participants, monitor the work and progress of trades, facilitate coordination, provide project documentation, and follow-up on details and issues until resolved. This maximizes the value our clients get for their construction budget.

## Construction Management services include:

- Professional on-site management
- Cost estimating and value analysis
- Effective communication
- Phasing alternatives/milestones
- Constructability reviews
- General conditions and contract requirements
- Bid/procurement process assistance
- Schedule development and schedule monitoring
- Leadership of coordination meetings
- Contract administration and management
- Quality assurance enforcement
- Progress and financial reporting
- Documentation and file maintenance
- Change order management
- Issue avoidance and dispute resolution
- Contract closeout supervision

