title: Architecture
banner: /static/images/banner/arch.jpg
key: arch
 
Our company offers a variety of services to meet your project’s needs, to take you from collaboration meetings all the way to ribbon-cutting and beyond. We believe that every project is unique, and can customize our approach to fit your particular project. While we are at the forefront of and specialize in design-build, we are very familiar with a number of delivery methods and are confident we can find the process that will best help you meet your goals.

##DELIVERY METHODS

**Design-Build Capability**

A primary focus of the business has been the management of design-build projects for both the private and public sectors. With consistent outstanding project results, Nova has been recognized as one of the region’s premier contractors in the specialized field of design-build contracting. A comprehensive understanding of the objectives and processes unique to design-build has allowed Nova to be a leader and innovator with design-build methodologies. As a result, the firm has been acknowledged with repeat contract awards from numerous clients including various educational institutions, government agencies, private developer clients.
With total design-build projects valued at over 85 crore completed or under construction during this short period of time, the Nova team has demonstrated exceptional ability to plan, design and construct in highly competitive markets.

**Project Experience**

Nova Construction has amassed an impressive resume encompassing a wide range of Commercial, Industrial, Institutional, Residential, and Energy projects, covering a variety of geographic locations and contracting markets, providing a broad foundation of experience. Project scopes range from single-story to mid-rise, with single project values over 50 crore. An extremely high success rate in competition for these projects demonstrates both technical and cost capabilities. The repeated awards and recognition that have been received, documents consistent outstanding performance and client satisfaction.

**PRECONSTRUCTION**

During the Preconstruction Phase, the objective of the Nova project team is to actively manage the budget development process, provide the Owner and its consultants with timely input on cost and method alternatives, and monitor the availability of materials and compatibility of costs. This process involves continuous monitoring and interaction.
Nova also takes an active role in monitoring, revising and updating the schedule for the preparation of contract documents by the design team. To accomplish this, a system of status reporting customized to reflect the mutual needs for current information is utilized to provide project cost and schedule accountability. 


*Schematic Phase*

- Assist in selection of project team
- Review building program; define project scope and objectives
- Review and coordinate consulting studies and reports
- Oversee constructability reviews

*Site Evaluation*

- Analyze the property line documentation. Cross check with the physical location the property line
- Analyze Soil Reports and identify hidden costs
- Verify water, sewage, electrical and telephone easements
- Identify flooding or storm drain concerns
- Verify zoning and environmental restrictions
- Identify out of property infrastructure costs.

*Cost Management/Estimating*

- Prepare value analysis reports for costs of alternate building systems and materials
- Perform constructability reviews and recommend cost effective alternatives
- Prepare feasibility budget
- Determine bonding requirements
- Analyze life cycle costs
- Prepare cost checks
- Prepare initial cash flow

*Design Development Phase*

- Coordinate with A&E/Client Team and Monitor Documents for Compliance
- Recommend alternate cost saving construction methods and materials with BIM Modeling
- Resolve spatial conflicts associated with the added level of detail in the shop/fabrication models.
- Provide very detailed and accurate shop installation drawings that coincide with coordinated shop models.
- Provide a visualization tool for field staff to help understand the design and installation of systems.
- Provide a tool to assist in scheduling and installation sequencing.
- Reduce Requests for Information and associated administrative time. 

*Time Management/Scheduling*

- Prepare initial milestone schedule
- Determine permitting, agency review & approval requirements
- Prepare phasing/sequencing schedule
- Prepare master project schedule
- Identify owner furnished and long lead items  

*Construction Document Phase*

- Organize field staff
- Design management reporting system, document control and submittal Schedule
- Continue document review and coordination
- Prepare quality control plan in coordination with architect and owner

*Bidding and Procurement Phase*

- Prepare instructions to bidders; bid Forms
- Conduct walk through and on site visits
- Mobilize field staff
- Implement submittal procedures
- Hold pre-bid conferences
- Solicit bids
- Award subcontracts
- Preconstruction conference
- Assist in obtaining permits.

##BUILDING INFORMATION MODELING

Nova Construction Company’s internal BIM Leadership Team directs and manages our BIM Working Team. The BIM Leadership Team is comprised of key members from both Nova, the owner’s representatives, architectural/engineering firms, and subcontractors. Our BIM department works with these firms to ensure the project BIM requirements are understood and delivered by all parties. Bringing experience, integrity, and extensive knowledge of BIM & IPD principles, we work together for optimal results.
The goals of BIM are to facilitate a collaborative project environment between all team members from project inception to completion and to execute coordinated project documents using the 3D modeling and parametric features of BIM. Using BIM will improve system coordination and the execution of design intent in the field to streamline construction processes. Below are some of the major goals and objectives for our use of BIM.


##CONSTRUCTION
We are a firm believer in partnering and the motivating force partnering can have on a project. Developing trust with a team where each member looks for ways to help the other, promotes the best kind of collaboration and each member benefits.
The construction staffing consists of an experienced team integrated into their projects during design and prior to construction commencing. We believe this up front staffing, more than pays for itself with the planning, coordination and thorough understanding of the projects before a single spade of dirt is turned. Subcontractor submittals, shop drawings, RFI’s and coordination questions are prioritized and are scheduled utilizing lean construction techniques and reverse planning scenarios. We require the subcontrac­tors to think out further than they are used to, get their buy in and hold them accountable for sequence, sched­ule, production and quality and memorialize these commitments with the subcontractor ownership, all before the start of construction.
We have weekly project foreman’s meetings and specialty subcontractor coordination meetings (as needed) and include the subcontractors PM’s to confirm the commitment of resources to the proj­ect. Realistic schedules are provided at a 6 week look ahead level coordinated by area, item and duration and all tying back to the master project schedule. Nova strongly encourages a relationship based team approach to commitments and believes that face time is the best way to get intended results. Our Superintendents and PM’s are highly skilled in the art of getting the most of a team member and genuinely consider their success. This is achieved by prudent planning, interaction, experience and respect.

##Cost Control

Budget accountability is the responsibility of the Project Managers. They manage the budget & work with the owners staff to review and control costs on a monthly basis or at shorter intervals as required. Our in house accounting department provides all cost reporting for our Project Managers and owner at all monthly reviews, make payments to subcontractors, as well as formalize request for payment to the University.
                              Quality of the Project
Quality control represents a mindset that is the responsibility of all members of the team. It is each individual’s duty to fully understand their area of responsibility and to be accountable for achieving the highest level of quality from the initial design to the final installation. 

##CORPORATE SAFETY

Nova Construction Company has developed a comprehensive Safety Program through consultation with Safety Experts, Insurance Agents and our own past experience. This program was designed to protect all of our employees, the subcontracted work force, inspectors, owner’s representatives, project visitors and the existing neighborhood. We are proud to say that we have an exemplary safety record and enjoy a preferred insurance rate due to our continuing safety excellence. 

##Safety Philosophy

A project’s “Best Value” can only be reached with superior safety performance. Nova’s corporate wide safety policy provides safety and incident prevention education and training, and enforcement for all employees, subcontractors, and visitors. Nova is also familiar with compliance with the stringent safety measures of OSHA and the Army Corps EM-385-1-1 regulations required on all military installations.
Additionally, our Project Manager, Project Superintendent, and QC Manager have also received current 30-Hour OSHA safety training and actively contribute to a safe workplace on a continuous basis. Safety violations will not be tolerated. A “Safety First” policy is always Nova’s standard requirement from initial construction activities, throughout the long term management. Our team will never sacrifice safety for production or profit.
Our firm has witnessed vast improvements in the quality of work products and the safe performance of project work phases since implementing our vastly upgraded and comprehensive safety program starting in 2016. Almost immediately, we experienced a reduction in the number of recordable injury and illness cases as well as a reduction in first aid cases. Our field offices have noted that this aggressive approach to safety has resulted in the following types of improvements: 
    - More worker involvement
    - Greater worker ownership in the safety program resulting in improved quality and productivity
    - Improved labor/management relations
    - Enhanced positive image of management leads to greater worker trust
    - Lowered incident rates, reduced worker compensation, and other related costs
    - Enhances the mission to assure continuous improvement
    - Improved motivation to work safely, leading to better quality, improved productivity, and a happier work force
    - Increased partnership and mentoring opportunities with DOE sites, OSHA and community companies
    - Improvement in lost workday case rates at sites, directly relating to real dollar savings
    - Created a positive “behavioral” impact where employees work safer at home as well as work
It is the intent of management and our on-site supervision to provide a safe and healthful working environment including establishment of safe work practices and continued training of our workforce and subcontractor’s employees. Our program has clearly demonstrated a substantial value added in terms of not only reduction of injuries, lost workdays, and the associated costs, but further enhancement of worker involvement in safety programs, partnership and improved management-worker relationship. 
Our Safety Program Includes the Following Highlights:
    - Project specific site safety plans
    - Supervisor and employee training programs
    - Construction equipment and operator training
    - Site specific and company new hire orientation
    - Daily site safety meetings
    - Site safety surveys and audits
    - Equipment / tool inspections
    - Rewards for outstanding safety performance
    - Mandatory subcontractor participation in safety program
    - Disciplinary action policies

##Safety Record

Nova is provided Worker’s Compensation Insurance through State Compensation Insurance Fund. The Experience Modifier Rate for the insurance is: 0.65. We receive favorable insurance rates based on our Comprehensive Experience Summary. Currently, Nova is proud to declare a zero loss run record for the past five years. 

##Summary

Nova Construction takes safety very seriously and does not make exceptions. We are outspoken ambassadors of better safety training, enforcement, and incentives at both the local and national levels. Nova Construction continues to work closely with the various industry associations, governmental agencies, and owners to create more consistent enforcement of Safety requirements. We work closely with our staff, our subcontractors, and even their workforce to make sure our safety philosophy is passed along to every project, and every employee.  Our entire company is committed to Safety Excellence and we believe we are an indisputable leader when it comes to safety. 


##SUSTAINABILITY

With extensive knowledge and experience in all areas of sustainable planning, design, construction, and operations, Nova Construction Company is a recognized sustainability contractor and leader in resource efficiency. Nova Construction has over 10 years of experience designing, building, and certifying LEED projects. Our commitment to creating positive improvements in the built environment, operational practices, and implementation of sustainability principles has enabled us to create awareness within the communities and organizations in which we serve. Our philosophy is centered on an innovative hands-on approach, in which we encourage active participation and foster ownership and amongst all team members. We believe that our success lies with the collaboration amongst the teams we serve and that the expression of knowledge and concepts is the best method for exceeding goals. 

##INTERNAL DESIGN MANAGEMENT DEPARTMENT

Nova Construction Company is somewhat unique with the organization and function of an Internal Design Management Department. This department is comprised of architectural and engineering professionals and is tasked with the management and coordination of the design process. The responsibilities of the department’s professional staff are to provide leadership and oversight to insure an on-time delivery of all design submittals, and confirm that all code and quality standards are met. Design quality control, discipline cross-checks, and value engineering are also an integral part of their project role.  

##CONSULTING ENGINEERING FIRM ALLIANCES

Nova Construction Company has formed strategic alliances with several consulting engineering firms. These established relationships and mutual commitment to overall success provides nova with the design resources to take any number of projects from conceptual proposal design to 100% completion. Nova Construction has established strong alliances with qualified Civil, Landscape, Structural, Mechanical, and Electrical Engineering firms, as well as numerous specialty consultants. Each firm is equally familiar with Nova Construction Company and highly qualified to flourish in the unique process inherent to design-build projects and the design-build environment. Nova’s design-build team is routinely comprised of architects and engineers that have worked together and successfully completed the design and engineering for over 30 design-build projects completed with Nova Construction. This combined experience and working familiarity provides added benefit throughout the proposal, preconstruction, design development, construction, and commissioning stages.
