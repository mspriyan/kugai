title: Interior Design
banner: /static/images/banner/interior.jpg
key: interior

We focus on thoroughly understanding your design aesthetic, personal values, and passions to create a singular, curated interior that is an authentic reflection of your unique taste and lifestyle

**Discovery**

Each project begins with a comprehensive discussion to identify your design needs, define your style, review design inspiration images and understand the overall space plan of your home.

**Inspiration**

Our design team creates a room-by-room concept statement that will help us guide the overall design direction throughout your project.

**Design**

We present fully developed design ideas and, based on your feedback, any necessary revisions are made and shown in a follow-up presentation where your home&#39;s final design is selected.


**Procurement**

After the overall design has been finalized, our in-house procurement team orders your new furnishings, fixtures and accessories.

**Installation**

The design team installs all furnishings, art and accessories in your home over the span of 1-5 days depending on the scale of the project.



**Big Reveal**

It&#39;s time to celebrate! We unveil the new interior, complete with a few special surprises and a bubbly toast to welcome you to your new home!




## OUR SERVICES

Whether you&#39;re considering refreshing your current home or constructing a new build, our team works diligently to ...

#### **SPACE PLANNING**

Your space will be beautiful, that is a certainty, and we will also ensure it flows well and functions efficiently. We partner with our clients to create a space plan that identifies movement through the home so we are able to expertly furnish each room.

#### **DESIGN MANAGEMENT**

From lighting design to selecting paint, finalizing flooring and curating accessories, there is quite a lot to manage and track during the construction process. Our team works closely with your builder to ensure your architectural materials match your final design plan.

#### **CUSTOM DRAPERY &amp; UPHOLSTERY**

One size rarely fits all. We partner with a local manufacturer to bring custom, heirloom-quality pieces to life that fit your space perfectly.

#### **SELECTION &amp; SCHEDULES**

Scheduling and communication are key elements to a successful project. We work with your builder and architect to coordinate schedules, provide selection details to all parties, and ensure your vision is effectively communicated from the beginning.

#### **PROCUREMENT &amp; INSTALLATION**

Our in-house procurement team handles the ordering and delivery of all your furnishings, art and accessories. We contact vendors on your behalf to make sure pieces arrive on time and undamaged. Our team is on-site during installation to guarantee everything looks picture perfect for the big reveal.

#### **CONTINUED CLIENT CARE**

Life is constantly evolving. We want your home to be a space that gives you the flexibility to grow with those changing needs. We love working with our clients to refresh a room or design nursery spaces as your family grows.


# How we work

NOVA leads a dynamic team that focuses on high-end residential interior projects. Grounded in the philosophy that your home tells your family&#39;s story, NOVA and her team thoughtfully balance observation, visualization, and curation in order to create singular interiors for their clients worldwide.

NOVA&#39;s design process is centered on discovering her client&#39;s values, learning what brings them personal joy, and then weaving those details throughout each space to tell their story. Throughout the design process, each client works with their own Senior Designer and Design Assistant from the NOVA team. They work hand in hand with their clients to create timeless, inspired spaces that bridge the gap between high-end design and functional living.