title: Landscaping
banner: /static/images/banner/scaping.jpg
key: landscaping

Nova landscaping provides excellence in a full spectrum of  Front-, side-, and rear-yard landscape installations , Rooftop decks, pergolas, planter boxes, and customized screens, Organic vegetable and food production spaces, Exterior lighting and drip irrigation

#### **SPACE PLANNING**

Your space will be beautiful, that is a certainty, and we will also ensure it flows well and functions efficiently. We partner with our clients to create a space plan that identifies movement through the place so we are able to expertly plan each and every area.

#### **DESIGN MANAGEMENT**

From lighting design to selecting paint, finalizing flooring and other accessories, there is quite a lot to manage and track during the construction process. Our team works closely with your builder to ensure your architectural materials match your final design plan.

#### **CUSTOM DRAPERY &amp; UPHOLSTERY**

One size rarely fits all. We partner with a local manufacturer to bring custom, heirloom-quality pieces to life that fit your space perfectly.

#### **SELECTION &amp; SCHEDULES**

Scheduling and communication are key elements to a successful project. We work with your builder and architect to coordinate schedules, provide selection details to all parties, and ensure your vision is effectively communicated from the beginning.

#### **PROCUREMENT &amp; INSTALLATION**

 Most of our designs are developed through a two-phase process complete with enhanced visual models and three-dimensional presentations to give our clients a clear picture of the finished product. Phase 1 focuses on the client&#39;s style, desired function, and rough budget. In Phase 2, we refine these details and select plants and materials based on the client&#39;s tastes and maintenance desires.

#### **CONTINUED CLIENT CARE**

Life is constantly evolving. We want your home to be a space that gives you the flexibility to grow with those changing needs. We love working with our clients to refresh a room or design nursery spaces as your family grows.

