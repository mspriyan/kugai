title: About Us

#### We value customer relationships and understand the importance of integrity.  Our team will work tirelessly to earn your trust and handle your project (large or small) with the utmost integrity. Our team members have over 9 combined years of experience and are the trusted, expert choice for your project. Each project is an opportunity to build a relationship while performing the highest quality of work.

Our staff has managed the construction of a variety of facilities for a multitude of clients: educational and municipal facilities, health care facilities, firehouses, community centers, libraries, industrial and wastewater treatment plants, bridges, roadways and housing projects to name a few.

The Nova&#39;s team of seasoned construction managers assigned to each project takes pride in the services they provide. Our management teams provide project leadership and maintain a focus on the construction schedule. They effectively communicate with the project participants, monitor the work and progress of trades, facilitate coordination, provide project documentation, and follow-up on details and issues until resolved. This maximizes the value our clients get for their construction budget.

**Our promise**

- Customer satisfaction
- Commitment to on-time work completion
- Highest quality materials
- Professional character
- Quality control

