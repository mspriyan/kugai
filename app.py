from flask import Flask,render_template
from flask_flatpages import FlatPages
from modern import Modern
from luxary import Luxary
from premium import Premium

DEBUG = True
FLATPAGES_AUTO_RELOAD = DEBUG
FLATPAGES_EXTENSION = '.md'

app = Flask(__name__)
app.config.from_object(__name__)
pages = FlatPages(app)


@app.route('/')
def index():
    return render_template('index.html')

# @app.route('/landing.html')
# def landing():
#     return render_template('landing.html')

@app.route('/landing/<what>/')
def landing(what):
    path = f"landing/{what}"
    page = pages.get_or_404(path)
    return render_template('landing.html', data=page)

@app.route('/about/')
def about():
    data = pages.get_or_404('about')
    return render_template('about.html', data=data)

@app.route('/pricing.html')
@app.route('/pricing_<type>.html')
def pricing(type=None):
    if type == "modern":
        payload = Modern()
    elif type == "premium":
        payload = Premium()
    elif type == "luxary":
        payload = Luxary()
    else:
        payload = Modern() + Premium() + Luxary() 
    return render_template('pricing.html', packages=payload)

@app.route('/details/<type>/<string:id>.html')
def details(type,id):
    path = f"{type}/{id}"
    page = pages.get_or_404(path)
    return render_template('details.html', data=page)

@app.route('/gallery/<type>/')
def gallery(type):
    return render_template('gallery.html', dest=type)

if __name__ == '__main__':
    app.run()